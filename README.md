# FPF Tech Challenge

Desafio da FPF para desenvolvedor

## Tecnologias
Projeto desenvolvido utilizando as tecnologias:

- Backend: TypeScript
- Frontend: Angular versão 13.0

## Execução

Primeiramente abre um terminal e rode `git clone https://gitlab.com/millerraycell/fpf-challenge.git`.

Depois rode `cd fpf-challenge`.

Para a próxima fase serão necessários dois terminais.

- No primeiro terminal que já estava aberto insira `cd backend/` e depois rode `yarn install` ou `npm install`, as dependências do backend do projeto estão sendo baixadas, pode demorar um pouco. Após a finalização do processo rode `yarn dev` ou `npm dev`. Pronto, agora o backend está rodando no seu computador.

- Abra mais um terminal na raiz do projeto, insira `cd frontend/` e depois rode `yarn install` ou `npm install`, as dependências do frontend do projeto estão sendo baixadas, pode demorar um pouco. Após a finalização do processo rode `yarn start` ou `npm start`. Pronto, agora o frontend está rodando no seu computador, agora no seu navegador acesse o link indicado para a aplicação.