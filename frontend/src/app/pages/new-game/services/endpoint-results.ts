export interface Ranking{
  player: string,
  date: Date,
  score: number
};

export interface Log{
  type:number,
  message: string
}

export interface StylesAttacks{
 'background-color': string,
 'justify-content': string
}

export interface StylesLifeBar{
  width: string,
 'background-color': string
}
