import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Ranking } from './endpoint-results';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  urlApi: string = 'http://localhost:3333'

  constructor(private http: HttpClient) { }

  postRanking(player: string, date: Date, score: number): Observable<Ranking>{
    const payload = {
      player,
      date,
      score
    };

    return this.http.post<Ranking>(this.urlApi + '/user-ranking', payload);
  }

  getRanking(){
    return this.http.get<Ranking[]>(this.urlApi + '/user-ranking');
  }
}
