import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game/game.component';
import { NewGameRoutingModule } from './ranking.routes';
import { PlayerComponent } from './player/player.component';
import { LifeBarComponent } from './game/life-bar/life-bar.component';



@NgModule({
  declarations: [
    GameComponent,
    PlayerComponent,
    LifeBarComponent
  ],
  imports: [
    CommonModule,
    NewGameRoutingModule
  ]
})
export class NewGameModule { }
