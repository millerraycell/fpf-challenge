import { ifStmt } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Log, StylesAttacks } from '../services/endpoint-results';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})

export class GameComponent implements OnInit{
  player: string = history.state.player;

  life_player: number = 100;
  life_monster: number = 100;

  special_monster: number = 1;
  special_player: number = 1;

  total_turns_player: number = 0;

  turn: boolean = true;

  log: Array<Log> = [];

  constructor(private service: ServicesService, private router: Router) { }

  ngOnInit(): void {
  }

  getAttackDamage(min: number, max:number) {
    return Math.floor(Math.random() * (max-min) + min);
  }

  BasicAttack(){
    let valueAttack = this.getAttackDamage(5,10);

    if(this.life_monster - valueAttack <= 0){
      this.turn = true;

      const score = Math.floor((this.life_player*1000)/this.total_turns_player);

      this.service.postRanking(this.player, new Date(), score).subscribe(x => x);

      window.alert(`Parabens ${this.player}!\nVocê Ganhou com ${score} pontos`);
      this.router.navigate(['ranking']);
    }

    this.log.unshift({type: 0, message: `${this.player} usou \"Ataque Básico\" (-${valueAttack} pontos de vida)"`});

    this.life_monster -= valueAttack;
    this.total_turns_player++;
    this.special_player++;
    this.turn = false;
  }

  SpecialAttack(){
    let valueAttack = this.getAttackDamage(10, 20);

    if(this.life_monster - valueAttack <= 0){
      this.turn = true;

      const score = Math.floor((this.life_player*1000)/this.total_turns_player);

      this.service.postRanking(this.player, new Date(), score).subscribe(x => x);

      window.alert(`Parabens ${this.player}!\nVocê Ganhou com ${score} pontos`);
      this.router.navigate(['ranking']);
    }

    this.log.unshift({type: 1, message: `${this.player} usou \"Ataque Especial\" (-${valueAttack} pontos de vida)"`});

    this.total_turns_player++;
    this.special_player = 1;

    const randomChance = Math.floor(Math.random() * (10-1) + 1);

    if(randomChance > 5){
      this.log.unshift({type: 2, message: `Monstro Atordoado`});
      this.turn = true;
    }
    else{
      this.turn = false;
      this.MonsterAttack();
    }

    this.life_monster -= valueAttack;
  }

  Heal(){
    if (this.life_player > 90){
      this.log.unshift({type: 3, message: `${this.player} usou \"Curar\" (+${100-this.life_player} pontos de vida)"`});
      this.life_player = 100;
    }
    else{
      this.log.unshift({type: 3, message: `${this.player} usou \"Curar\" (+10 pontos de vida)"`});
      this.life_player += 10;
    }

    if(this.life_player<= 0){
      window.alert("Fim de Jogo\nVocê Perdeu");
      this.router.navigate(['player']);
    }

    if(this.life_monster <= 0){
      this.turn = true;

      const score = Math.floor((this.life_player*1000)/this.total_turns_player);

      this.service.postRanking(this.player, new Date(), score).subscribe(x => x);

      window.alert(`Parabens ${this.player}!\nVocê Ganhou com ${score} pontos`);
      this.router.navigate(['ranking']);
    }


    this.total_turns_player++;
    this.special_player++;
    this.turn = false;
  }

  MonsterAttack(){
    setTimeout(() => {
      let valueAttack;

      if(this.special_monster === 3){
        valueAttack = this.getAttackDamage(8, 16);

        if(this.life_player - valueAttack <= 0){
          window.alert("Fim de Jogo\nVocê Perdeu");
          this.router.navigate(['player']);
        }

        this.log.unshift({type: 4, message : `Monstro usou \"Ataque Especial\" (-${valueAttack} pontos de vida)"`});

        this.special_monster=1;
      }

      else{
        valueAttack = this.getAttackDamage(6, 12);

        if(this.life_player - valueAttack <= 0){
          window.alert("Fim de Jogo\nVocê Perdeu");
          this.router.navigate(['player']);
        }

        this.log.unshift({type: 4, message : `Monstro usou \"Ataque Basico\" (-${valueAttack} pontos de vida)"`});

        this.special_monster++;
      }

      this.life_player -= valueAttack;
      this.turn = true;
    }, 500);
  }

  GiveUp(){
    window.alert("Muito obrigado por jogar!");
    this.router.navigate(['home']);
  }

  ApplyStyle(value: number){
    let style: StylesAttacks= {
      'background-color': '',
      'justify-content': ''
    };

    if (value === 0){
      style['background-color'] = '#6275b5';
      style['justify-content'] = 'flex-start';
    }
    if (value === 1){
      style['background-color'] = '#fdd685';
      style['justify-content'] = 'flex-start';
    }
    if (value === 2){
      style['background-color'] = '#ebebeb';
      style['justify-content'] = 'flex-end';
    }
    if (value === 3){
      style['background-color'] = '#62b56c';
      style['justify-content'] = 'flex-start';
    }
    if (value === 4){
      style['background-color'] = '#d87c7c';
      style['justify-content'] = 'flex-end';
    }
    return style;
  }

}
