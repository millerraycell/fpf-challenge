import { Component, Input, OnInit } from '@angular/core';
import { StylesLifeBar } from '../../services/endpoint-results';

@Component({
  selector: 'app-life-bar',
  templateUrl: './life-bar.component.html',
  styleUrls: ['./life-bar.component.css']
})
export class LifeBarComponent implements OnInit {
  @Input() life: number = 0;
  @Input() name: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  applyStyle(){
    let style :StylesLifeBar = {
      'width': `${this.life}%`,
      'background-color': ''
    };

    if(this.life >= 50){
      style['background-color'] = 'green'
    }
    if(this.life < 50 && this.life >= 21){
      style['background-color'] = 'yellow'
    }
    if(this.life <= 20){
      style['background-color'] = 'red'
    }

    return style;
  }

}
