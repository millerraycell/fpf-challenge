import { Component, OnInit } from '@angular/core';
import { RankingServicesService } from '../services/services.service';
import { Ranking } from '../services/type';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {
  ranking: Ranking[] = [];

  constructor(private service: RankingServicesService) { }

  ngOnInit(): void {
    this.service.getRanking().subscribe(x =>{
      this.ranking = x;

      this.ranking.map(x => {
        x.date = new Date(x.date).toLocaleString()
      })
    })
  }

}
