export interface Ranking{
  player: string,
  date: string,
  score: number
}
