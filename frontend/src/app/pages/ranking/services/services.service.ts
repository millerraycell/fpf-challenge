import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Ranking } from './type';

@Injectable({
  providedIn: 'root'
})
export class RankingServicesService {
  urlApi: string = 'http://localhost:3333'

  constructor(private http: HttpClient) { }

  getRanking(){
    return this.http.get<Ranking[]>(this.urlApi + '/user-ranking');
  }
}
