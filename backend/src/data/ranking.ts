interface PlayerRanking{
    player: string,
    date: Date,
    score: number
}

const Ranking: PlayerRanking[] = []

export default Ranking;