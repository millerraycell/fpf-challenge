import { Request, Response } from "express";
import Ranking from "../data/ranking";

class UsersRanking{
    create(request: Request, response: Response){
        console.log(request.body)
        const { player, date, score } = request.body;

        Ranking.push({player, date, score});

        return response.status(201).json({player, date, score});
    }

    index(request: Request, response: Response){
        const result = Ranking.sort((a,b) => {
            return b.score - a.score
        });

        return response.json(result);
    }
}

export default UsersRanking;