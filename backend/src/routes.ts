import express from 'express';
import UsersRanking from './controllers/UsersRankingController';

const router = express.Router();

const usersRanking = new UsersRanking();


router.post('/user-ranking', usersRanking.create);
router.get('/user-ranking', usersRanking.index);

export default router;